const url = "https://covid19.mathdro.id/api";


let app = angular.module('myApp', []);
app.controller("myCtrl", ($scope, $http) => {
    $scope.title = "Stay Home Stay Safe";

    /* $scope.changeValue = () => {
         $scope.title = "This is Home Time";
     };

    console.log('page loading...'); */

    $http.get(url).then(
        (response) => {
            console.log('Inside Success')
            console.log(response.data);
            $scope.all_data = response.data;
        },
        (error) => {
            console.log('error')
        }
    );
    //Get Country Data
    $scope.get_c_data = () => {
        let country = $scope.c;
        if (country == "") {
            $scope.c_data = undefined;
            return;
        }
        $http.get(`${url}/countries/${country}`).then((response) => {
                console.log(response.data);
                $scope.c_data = response.data
            },
            (error) => {
                console.log('error')
            }
        );
    };
});